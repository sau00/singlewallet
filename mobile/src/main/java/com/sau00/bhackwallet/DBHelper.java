package com.sau00.bhackwallet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by sau00 on 04.12.15.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "bhackSms", null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.d("SQL", "---Creating database---");

        db.execSQL("create table transactions (" +
                "id integer primary key autoincrement," +
                "card text," +
                "operator text," +
                "type text," +
                "currency text," +
                "amount real," +
                "balance real," +
                "timestamp integer" +
                ");");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
