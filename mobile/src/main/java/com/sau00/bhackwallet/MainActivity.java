package com.sau00.bhackwallet;

import android.content.Entity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        dbHelper = new DBHelper(this);
//
//        SQLiteDatabase db = dbHelper.getWritableDatabase();

//        Cursor c = db.rawQuery("select * from transactions", null);
//        ArrayList<Entry> entries = new ArrayList<>();
//
//        if(c.moveToFirst()) {
//            while(!c.isAfterLast()) {
////                int id = c.getColumnIndex("id");
//                Log.d("SELEQT", c.getString(c.getColumnIndex("id")));
//                entries.add(new BarEntry(Float.parseFloat(c.getString(c.getColumnIndex("amount"))), Integer.parseInt(c.getString(c.getColumnIndex("id")))));
//                c.moveToNext();
//            }
//
//            PieDataSet dataSet = new PieDataSet(entries, "# of calls");
//            ArrayList<String> labels = new ArrayList<String>();
//            labels.add("Label1");
//            labels.add("Label2");
//            labels.add("Label3");
//            labels.add("Label4");
//            labels.add("Label5");
//            labels.add("Label6");
//            labels.add("Label7");
//            labels.add("Label8");
//            labels.add("Label9");
//            labels.add("Label10");
//
//            PieChart chart = new PieChart(this);
//            setContentView(chart);
//
//            PieData data = new PieData(labels, dataSet);
//            chart.setData(data);
//
//            chart.setDescription("Hello");
//        }

//        countTransactions();

//        moneyTransactions();

        balanceTransactions();

        TransactionsParser tp = new TransactionsParser(getRawSMSList(), dbHelper);
        tp.parse();
    }

    public void moneyTransactions() {
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.rawQuery("select id, operator, sum(abs(amount)) from transactions group by operator order by count(operator) DESC", null);
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();

        if(c.moveToFirst()) {
            while(!c.isAfterLast()) {
                Log.d("DATABASE", c.getString(0) + " " + c.getString(2));

                entries.add(new Entry(Float.parseFloat(c.getString(2)), Integer.parseInt(c.getString(0))));
                labels.add(c.getString(1));

                c.moveToNext();
            }

            PieDataSet dataSet = new PieDataSet(entries, "Payments Systems");
            dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

            PieChart chart = new PieChart(this);
            setContentView(chart);

            PieData data = new PieData(labels, dataSet);
            chart.setData(data);

            chart.setDescription("Total money amount");
        }
    }

    public void balanceTransactions() {
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.rawQuery("select id, operator, balance from transactions where balance > 0 group by operator order by timestamp DESC", null);
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();

        if(c.moveToFirst()) {
            while(!c.isAfterLast()) {
                Log.d("DATABASE", c.getString(0) + " " + c.getString(2));

                entries.add(new Entry(Float.parseFloat(c.getString(2)), Integer.parseInt(c.getString(0))));
                labels.add(c.getString(1));

                c.moveToNext();
            }

            PieDataSet dataSet = new PieDataSet(entries, "Payments Systems");
            dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

            PieChart chart = new PieChart(this);
            setContentView(chart);

            PieData data = new PieData(labels, dataSet);
            chart.setData(data);

            chart.setDescription("Total money amount");
        }
    }

    public void countTransactions() {
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.rawQuery("select id, operator, count(operator) from transactions group by operator order by count(operator) DESC", null);
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();

        if(c.moveToFirst()) {
            while(!c.isAfterLast()) {
                Log.d("DATABASE", c.getString(0) + " " + c.getString(1));

                entries.add(new Entry(Integer.parseInt(c.getString(2)), Integer.parseInt(c.getString(0))));
                labels.add(c.getString(1));

                c.moveToNext();
            }

            PieDataSet dataSet = new PieDataSet(entries, "Payment Systems");
            dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

            PieChart chart = new PieChart(this);
            setContentView(chart);

            PieData data = new PieData(labels, dataSet);
            chart.setData(data);

            chart.setDescription("Transactions amount");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void parseSms(View view) {
        TextView stat = (TextView) findViewById(R.id.TextViewStats);
    }

    public List<HashMap<String, String>> getRawSMSList(){
        List<HashMap<String, String>> SMSList = new ArrayList<HashMap<String, String>>();
        Uri uriSMSURI = Uri.parse("content://sms/inbox");
        Cursor cur = getContentResolver().query(uriSMSURI, null, null, null, null);

        while (cur.moveToNext()) {
            String address = cur.getString(cur.getColumnIndex("address"));
            String date = cur.getString(cur.getColumnIndex("date"));
            String body = cur.getString(cur.getColumnIndexOrThrow("body"));

            HashMap<String, String> sms = new HashMap<String, String>();
            sms.put("address", address);
            sms.put("date", date);
            sms.put("body", body);

            SMSList.add(sms);
        }

        return SMSList;
    }
}
