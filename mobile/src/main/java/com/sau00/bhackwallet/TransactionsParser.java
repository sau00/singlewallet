package com.sau00.bhackwallet;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sau00 on 05.12.15.
 */
public class TransactionsParser {

    private DBHelper dbHelper;

    private List<HashMap<String, String>> transactions;

    private enum Operators {
        UNDETECTED, SBER, TINKOFF, QIWI
    }

    private List<HashMap<String, String>> rawSMSList;

    public TransactionsParser(List<HashMap<String, String>> rawSMSList, DBHelper dbHelper) {
        this.dbHelper = dbHelper;
        this.rawSMSList = rawSMSList;
    }

    private void debug() {
        for (HashMap<String, String> sms : rawSMSList) {
            Log.d("MyApplication", sms.get("date"));
        }
    }

    public void parse() {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        for (HashMap<String, String> sms : rawSMSList) {
            HashMap<String, String> transaction = new HashMap<>();

            ContentValues cv = new ContentValues();

            Operators operator = this.detectOperator(sms.get("address"));

            HashMap<String, String> smsData = parseSMSBody(sms.get("body"), operator);
            cv.put("card", smsData.get("card"));
            cv.put("operator", getOperatorCode(operator));
            cv.put("type", smsData.get("type"));
            cv.put("currency", "RUB");
            cv.put("amount", smsData.get("amount"));
            cv.put("balance", smsData.get("balance"));
            cv.put("timestamp", Long.toString(Math.round(Long.parseLong(sms.get("date")) / 1000)));

            // Добавляем только те записи, у которых есть значение перевода транзакции
            if(cv.get("amount") != null) {
                Cursor c = db.rawQuery("select * from transactions where timestamp = " +
                        cv.get("timestamp") + " and operator = '" + cv.get("operator") + "' and amount = " + cv.get("amount"), null);

                // Если записи не существует, добавляем ее в базу
                if(c.moveToFirst() == false) {
                    long rowId = db.insert("transactions", null, cv);
                    Log.d("SMSDATA", rowId + "_");
                    Log.d("SMSDATA2", cv.toString());
                }
            }
        }
    }

    // Детектим оператора по номерам
    private Operators detectOperator(String address) {
        Operators operator;

        if(address.equals("900")) {
            operator = Operators.SBER;
        } else if(address.equals("Tinkoff") || address.equals("+79018185010") ||
                address.equals("+79018185182") || address.equals("+79018185563") ||
                address.equals("+79018185409")) {
            operator = Operators.TINKOFF;
        } else if(address.equals("QIWIWallet") || address.equals("QIWI Wallet")) {
            operator = Operators.QIWI;
        } else {
            operator = Operators.UNDETECTED;
        }

        return operator;
    }

    private String getOperatorCode(Operators operator) {
        String operatorCode;

        switch (operator) {
            case SBER:
                operatorCode = "SBER";
                break;

            case TINKOFF:
                operatorCode = "TINKOFF";
                break;

            case QIWI:
                operatorCode = "QIWI";
                break;

            default:
                operatorCode = "";
                break;
        }

        return operatorCode;
    }
    // У сбера: Покупка, Списание, Выдача, Баланс, Перевел
    // У тинькова: Operatsiya, Popolnenie. Pokupka
    // У Киви: Oplata po karte, Kod dlya oplati scheta
    private HashMap<String, String> parseSMSBody(String SMSBody, Operators operator) {
        HashMap<String, String> SMS = new HashMap<String, String>();

        switch(operator) {
            case SBER:
//                Log.d("SMSTEXT", SMSBody);
                break;

            case TINKOFF:
                Log.d("TINKOFF", SMSBody);

                Pattern typePattern = Pattern.compile("(Pokupka|Popolnenie|Operatsiya)");
                Matcher typeMatch = typePattern.matcher(SMSBody);
                typeMatch.find();
                SMS.put("type", typeMatch.group(1));

                Pattern cardPattern = Pattern.compile("(Karta) \\*(\\d{4})");
                Matcher cardMatch = cardPattern.matcher(SMSBody);
                cardMatch.find();
                SMS.put("card", cardMatch.group(2));

                Pattern summPattern = Pattern.compile("(Summa) (\\d+.\\d+)");
                Matcher summMatch = summPattern.matcher(SMSBody);
                summMatch.find();

                if(typeMatch.group(1).equals("Popolnenie")) {
                    SMS.put("amount", summMatch.group(2));
                } else {
                    SMS.put("amount", "-" + summMatch.group(2));
                }

                Pattern amountPattern = Pattern.compile("(Dostupno) (\\d+.\\d+)");
                Matcher amountMatch = amountPattern.matcher(SMSBody);
                amountMatch.find();
                SMS.put("balance", amountMatch.group(2));

//                Pattern currencyPattern = Pattern.compile("(RUB|USD)");
//                Matcher currMatcher = currencyPattern.matcher(SMSBody);
//                while (currMatcher.find()) {
//                    Log.d("TINKOFF", "Currency:" + currMatcher.group(1));
//                }
                break;

            case QIWI:
                Pattern p = Pattern.compile("Kod dlja oplaty");
                Matcher m = p.matcher(SMSBody);

                Pattern p2 = Pattern.compile("Oplata po karte");
                Matcher m2 = p2.matcher(SMSBody);

                // Узнаем тип SMS-уведомления:
                if(m.find()) {
                    Pattern p3 = Pattern.compile("Kod dlja oplaty scheta (.*?) na summu");
                    Matcher m3 = p3.matcher(SMSBody);
                    m3.find();
                    SMS.put("type", m3.group(1));

                    Pattern p4 = Pattern.compile("na summu (.*?) RUB");
                    Matcher m4 = p4.matcher(SMSBody);
                    m4.find();
                    SMS.put("amount", "-" + m4.group(1));

                    SMS.put("card", "QIWI");
                    SMS.put("balance", "0");

                } else if(m2.find()) {
                    Pattern p3 = Pattern.compile("Oplata po karte (.*?) SUM");
                    Matcher m3 = p3.matcher(SMSBody);
                    m3.find();
                    SMS.put("card", m3.group(1));

                    Pattern p4 = Pattern.compile("SUM: (.*?) RUB");
                    Matcher m4 = p4.matcher(SMSBody);
                    m4.find();
                    SMS.put("amount", "-" + m4.group(1));

                    Pattern p5 = Pattern.compile("BAL: (.*?) RUR");
                    Matcher m5 = p5.matcher(SMSBody);
                    m5.find();
                    SMS.put("balance", m5.group(1));

                    Pattern p6 = Pattern.compile(":[0-9]{2}. (.*)");
                    Matcher m6 = p6.matcher(SMSBody);
                    m6.find();
                    SMS.put("type", m6.group(1));
                }
                break;

            default:
                break;
        }

        return SMS;
    }

    // Позднее сюда надо будет подрубать классы для каждого оператора отдельно
    private void detectTransactions() {

    }
}
